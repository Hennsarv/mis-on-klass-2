﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HennuSysteem
{

    enum Sugu { Naine=1, Mees=2, Tundmata = 99}

    class Person  // class e. andmetüüp
    {
        public string Name; // vaikimisi ""
        public int Age;     // vaikimisi 0
        public Sugu Gender; // vaikimisi 0
        public Person Kaasa; // vaikimisi null (e. puudub)

        public void Abielu(Person kaasa)
        {
            if (Kaasa == null && kaasa.Kaasa == null)
            {
                Kaasa = kaasa;
                kaasa.Kaasa = this;
            }
        }


        public override string ToString()
        {
            return $"{Gender} {Name} vanusega {Age} tal on paariline {(Kaasa?.Name)?? "puudub"}";
        }
    }

    class Program
    {
        public static void Main(string[] args)
        {
            int[] arvud = new int[3];
            foreach (var x in arvud) Console.WriteLine(x);

            List<Person> valvurid = new List<Person>();
            valvurid.Add(new Person { });
            foreach (var x in valvurid) Console.WriteLine(x.Name);


            List<Person> inimesed = new List<Person>();
            inimesed.Add(new Person { Name = "Henn", Age = 62, Gender = Sugu.Mees });
            inimesed.Add(new Person { Name = "Ants", Age = 40, Gender = Sugu.Mees });
            inimesed.Add(new Person { Name = "Pille", Age = 28, Gender = Sugu.Naine });
            inimesed.Add(new Person { Name = "Malle", Age = 30, Gender = Sugu.Naine });
            inimesed.Add(new Person { Name = "Kriksadull", Age = 28, Gender = Sugu.Tundmata });

            inimesed[1].Abielu(inimesed[3]);
            inimesed[1].Abielu(inimesed[2]);


            foreach (var x in inimesed) Console.WriteLine(x);
            int mitu = 0;
            double summa = 0;
            foreach (var x in inimesed) summa += x.Age;
            Console.WriteLine($"keskmine vanus {summa/inimesed.Count}");

            double meesteSumma = 0;
            int meesteMitu = 0;
            double naisteSumma = 0;
            int naisteMitu = 0;

            // variant switchiga
            foreach (var x in inimesed)
            {
                switch(x.Gender)
                {
                    case Sugu.Naine:
                        naisteMitu++;
                        naisteSumma += x.Age;
                        break;
                    case Sugu.Mees:
                        meesteMitu++;
                        meesteSumma += x.Age;
                        break;
                }
            }

            // variant iffiga
            foreach (var x in inimesed)
            {
                if (x.Gender == Sugu.Naine)
                {
                    naisteMitu++; naisteSumma += x.Age;
                }
                else if (x.Gender == Sugu.Mees)
                {
                    meesteMitu++; meesteSumma += x.Age;
                }
            }


            Console.WriteLine($"naiste keskmine {naisteSumma / naisteMitu}");
            Console.WriteLine($"meeste keskmine {meesteSumma / meesteMitu}");


            // proovime sama asja massiivina

            Person[] persoonid = new Person[10];
            for (int i = 0; i < inimesed.Count; i++)
            {
                persoonid[i] = inimesed[i];
            }

            summa = 0;
            mitu = 0;
            foreach(var x in persoonid)
            {
                if (x != null)
                {
                    summa += x.Age;
                    mitu++;
                    Console.WriteLine(x);
                }
            }
            Console.WriteLine(summa/mitu);
 
        }
    }
}
